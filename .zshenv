# Variables which are not safe for public use (for example, environmental variables containing user personal information)
source "${XDG_CONFIG_HOME}/zsh/notpublicenv"

export EDITOR="nvim"
export VISUAL="nvim"

export PATH="${PATH}:${HOME}/.local/bin"
# GO enviromental variables
export GOPATH="${HOME}/.local/share/go"
export GOMODCACHE="${HOME}/.local/share/go/pkg/mod"

