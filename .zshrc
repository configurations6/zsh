#Spaceship prompt
source "${XDG_CONFIG_HOME}/zsh/plugins/spaceship/spaceship.zsh"
source "${XDG_CONFIG_HOME}/zsh/plugins/spaceship-vi-mode/spaceship-vi-mode.plugin.zsh"

source "${XDG_CONFIG_HOME}/zsh/plugins/zsh-completions/zsh-completions.plugin.zsh"
# The following lines were added by compinstall
zstyle :compinstall filename "${XDG_CONFIG_HOME}/zsh/.zshrc"

autoload -Uz compinit
compinit
# End of lines added by compinstall
HISTFILE=${XDG_CONFIG_HOME}/zsh/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd extendedglob
unsetopt beep
bindkey -v

# Spaceship configuration
SPACESHIP_ASYNC_SHOW=false
spaceship add --before char vi_mode
eval spaceship_vi_mode_enable

# ALIASES

# sudo command
alias sudo="sudo -E"

# ls command
alias ls="ls --color=auto"
alias la="ls -a"
alias ll="ls -alFh"
alias l.="ls -A | grep -e '^\.'"

# grep command
alias grep="grep --color=auto"

# df command
alias df="df -h"

# free command
alias free="free -ht"

# tmux
alias tm="tmux new -A -s main"
alias tmn="tmux new -A"

# extractor
ex()
{
	if [ -f $1 ] ; then
		case $1 in
			*.tar.bz2)	tar xjf $1	;;
			*.tag.gz)	tar xzf $1	;;
			*.bz2)		bunzip2	$1	;;
			*.rar)		unrar x $1	;;
			*.gz)		gunzip $1	;;
			*.tar)		tar xf $1	;;
			*.tbz2)		tar xjf $1	;;
			*.tgz)		tar xzf $1	;;
			*.zip)		unzip $1	;;
			*.Z)		uncompress $1	;;
			*.7z)		7z x $1		;;
			*.deb)		ar x $1		;;
			*.tar.xz)	tar xf $1	;;
			*.tar.zst)	tar xf $1	;;
			*)		echo "'$1' can not be extracted via ex()";;
		esac
	else
		echo "'$1' is not a valid file"
	fi
}

source "${XDG_CONFIG_HOME}/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
